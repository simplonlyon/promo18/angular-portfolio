import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { SessionComponent } from './session/session.component';
import { SingleStudentComponent } from './single-student/single-student.component';

const routes: Routes = [

  {path: '', component: HomeComponent},
  {path:'session/:session', component: SessionComponent},
  {path: 'student/:id', component: SingleStudentComponent},
  {path: '**', component: NotFoundComponent}, //Wildcard à mettre à la fin forcément des routes

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
