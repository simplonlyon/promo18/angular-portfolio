import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, tap } from 'rxjs';
import { Student } from '../entities';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class SessionComponent implements OnInit {
  students:Student[] = [];
  session:string = '';

  constructor(private route:ActivatedRoute, private service:StudentService) { }

  ngOnInit(): void {
    this.route.params.pipe(
      tap(params => this.session = params['session']),
      switchMap(params => this.service.getBySession(params['session']))
    ).subscribe(data => this.students = data);
  }

}
