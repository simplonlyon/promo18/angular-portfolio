import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Student } from './entities';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Student[]>(environment.serverUrl+'/api/student');
  }

  getOne(id:number) {
    return this.http.get<Student>(environment.serverUrl+'/api/student/'+id);
  }

  getBySession(session:string) {
    return this.http.get<Student[]>(environment.serverUrl+'/api/student?session='+session);

  }
}
