export interface Student {
    id?:number;
    name:string;
    mail:string;
    session:string;
    projects:Project[];
}

export interface Project {
    id?:number;
    name:string;
    techs:string;
    picture:string;
    student?:Student;
}