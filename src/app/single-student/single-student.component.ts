import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs';
import { Student } from '../entities';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-single-student',
  templateUrl: './single-student.component.html',
  styleUrls: ['./single-student.component.css']
})
export class SingleStudentComponent implements OnInit {
  student?:Student;
  constructor(private service:StudentService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    /**
     * En gros ici, on récupère l'id de la route et on s'en sert pour faire un appel
     * vers /api/student/{id} en lui donnant l'id qu'on a récupéré dans la route.
     * Histoire de faire la manière la plus "optimisée" on utilise ici le pipe et le
     * switchMap des observable, c'est assez complexe à expliquer et à comprendre,
     * mais en soit, ce petit bout de code, vous pouvez le réutiliser presque tel quel
     * pour toutes les pages où vous avez besoin de récupérer une donnée par son id
     */
    this.route.params.pipe(
      switchMap(params => this.service.getOne(params['id']))
    )
    .subscribe(data => this.student=data);
  }

}
