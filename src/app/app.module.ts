import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from "@angular/common/http";
import { StudentCardComponent } from './student-card/student-card.component';
import { SingleStudentComponent } from './single-student/single-student.component';
import { ProjectCardComponent } from './project-card/project-card.component';
import { SessionComponent } from './session/session.component';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StudentCardComponent,
    SingleStudentComponent,
    ProjectCardComponent,
    SessionComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
